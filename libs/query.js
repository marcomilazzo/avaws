var pg 		= require("pg");
var config 	= require("../config");

pg.defaults.poolSize = 100;
var pool = new pg.Pool(config.postgres);
var cnPool = new pg.Pool(config.postgrescentralcn);

module.exports = {
	query: function( sql, values, callback ) {
		pool.connect( function ( err, client, done ) {
			if ( err ) {
				return callback( err, null );
			}
			client.query(sql, values, function ( err, result ) {
				done();
				if ( err ) {
					return callback( err, null );
				}
				return callback( null, result );
			});
		})
	},
	cnQuery: function( sql, values, callback ) {
		cnPool.connect( function ( err, client, done ) {
			if ( err ) {
				return callback( err, null );
			}
			client.query(sql, values, function ( err, result ) {
				done();
				if ( err ) {
					return callback( err, null );
				}
				return callback( null, result );
			});
		})
	}
}
