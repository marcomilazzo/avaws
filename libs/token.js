var config 	= require("../config");
var jwt 	= require("jsonwebtoken");

module.exports.generateToken = function ( objectToTokenize ) {
	var sign = jwt.sign( objectToTokenize, config.secret );
	return sign;
}
module.exports.verifyToken = function ( tokenToVerify, callback ) {
	jwt.verify( tokenToVerify, config.secret, function ( err, decoded ) {
		if ( err ) {
			return callback( err, null );
		}
		return callback( null, decoded );
	})
}