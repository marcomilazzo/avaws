var movementsController = require( "../controllers/movements" );
var settingsController 	= require("../controllers/settings");
module.exports.set = function ( router ) {
	router.get( "/sync/settings/list", settingsController.getPreferenceForSync );
	router.get( "/sync/movements/list", movementsController.getMovementsListForSync );
	router.get( "/settings/panels/list", settingsController.getPanelList );
	router.get( "/settings/generalSuppliers/:iso", settingsController.getGeneralSupplier );
}