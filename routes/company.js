var companyController = require("../controllers/companyController");
module.exports.set = function ( router ) {
	// get 
	router.get( "/company/search/:term", companyController.searchCompany );
	router.get( "/company/:primary", companyController.getCompanyDetails );
}