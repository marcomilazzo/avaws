var controller 	= require("../controllers/searchController");
var jwt 		= require("../libs/token");
var config 		= require("../config.js");
module.exports.set = function ( router ) {

	router.use ( function ( req, res, next ) {
		var token = req.params.token || req.query.token || req.headers['x-access-token'];

		if ( !token ) {
			res.json(
			403,
			{
				success: false,
				message: "no token provided"
			});
		} else {
			jwt.verifyToken( token, function ( err, decoded ) {
				if ( err ) {
					return res.json( { success: false, message: "token is invalid" } );
				} else {
					//res.json( { success: true, message: "token is valid" } );
					next();
				}
			});
		}
	});
	router.get("/products/search/:searchterm", controller.srcSearchProduct);
	router.post("/products/search/advanced", controller.srcSearchProductAdvanced);
	router.get("/products/detail/:product", controller.srcSearchProductsDetails);
	router.get("/products/search", controller.srcIndex);
	router.get("/products/:scope/sidebar/:primary", controller.srcProductSidebar);
	router.get("/products/product/same/:scope/:primary", controller.srcSameProduct);
}
