var avabrowser 	= require("../controllers/avabrowserController");

module.exports.set = function ( router ) {
	// GET

	// resultmask
	router.get("/avabrowser/:iso/resultmask/:primary", avabrowser.getResultMask);
	router.get("/avabrowser/:iso/resultmask", avabrowser.getResultMask);
	// sidebar
	router.get("/avabrowser/:iso/sidebar/:primary", avabrowser.getSidebar);
	router.get("/avabrowser/:iso/sidebar", avabrowser.getSidebar);
	// command
	router.get("/avabrowser/:iso/command", avabrowser.getCommand);
	// properties ( resultmask and sidebar )
	router.get("/avabrowser/:iso/:scope/properties", avabrowser.getProperties);

	// POST ( update )
	router.post("/avabrowser/:iso/resultmask/:primary", avabrowser.updateResultMask);
	router.post("/avabrowser/:iso/relation/:primary", avabrowser.updateSidebar);

	// PUT ( create )
	router.put("/avabrowser/:iso/resultmask/newresultmask", avabrowser.storeResultMask);
	router.put("/avabrowser/:iso/relation/newsidebar", avabrowser.storeSidebar);

}