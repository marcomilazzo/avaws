module.exports.set = function ( router ) {
	// get
	router.get( "/group/list", function ( req, res, next ) {
		return res.json(200, []);
	});
	router.get( "/group/:primary", function ( req, res, next ) {
		return res.json(200, {});
	});
	router.get( "/group/:primary/customers", function ( req, res, next ) {
		return res.json(200, []);
	});

	// update 
	router.post( "/group/:primary", function ( req, res, next ) {
		return res.json(200, {});
	});

	// create
	router.put( "/group", function ( req, res, next ) {
		return res.json(200, {});
	});
}