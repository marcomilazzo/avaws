var userController = require("../controllers/userController");
module.exports.set = function ( router ) {
	// get
	router.get( "/user/list", userController.getUserList );
	router.get( "/user/:primary", userController.getUserList );

	// update 
	router.post( "/user/:primary", function ( req, res, next ) {
		return res.json(200, {});
	});

	// create
	router.put( "/user", function ( req, res, next ) {
		return res.json(200, {});
	});
}