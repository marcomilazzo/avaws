var deviceController = require("../controllers/deviceController");
module.exports.set = function ( router ) {
	router.get( "/settings/device/list", deviceController.getDeviceList );
	router.get( "/settings/device/:primary", deviceController.getDevice );

	router.post( "/settings/device/:primary", deviceController.updateDevice );

	router.put( "/settings/device", deviceController.storeDevice );
}