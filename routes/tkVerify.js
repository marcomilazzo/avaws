var tkVerifyController 	= require( "../controllers/tkVerify" );
module.exports.set = function ( router ) {
	router.post( "/api/tk/verify", tkVerifyController.verifyTokenIdentity );
}