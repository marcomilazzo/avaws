var userController = require("../controllers/userController");
module.exports.set = function ( router ) {
	// get
	router.get( "/customer/list",  userController.getCustomerList );
	router.get( "/customer/:primary", userController.getCustomerList );

	// update 
	router.post( "/customer/:primary", function ( req, res, next ) {
		return res.json(200, {});
	});

	// create
	router.put( "/customer", function ( req, res, next ) {
		return res.json(200, {});
	});
}