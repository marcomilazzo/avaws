var searchRouter 	= require("./search");
var userRouter 		= require("./user");
var syncRouter 		= require("./sync");
var avabrowser 		= require("./avabrowser");
var users 			= require("./users");
var customers 			= require("./customer");
var tkVerRouter 	= require("./tkVerify");
var config 			= require("../config");
var jwt 			= require("jsonwebtoken");
// get this filename
var basename		= module.filename;
var fs 				= require("fs");
var path 			= require("path");

module.exports.set = function ( router ) {
	router.get("/", function ( req, res, next ) {
		var response = {
			appname : "avaws",
			description: "RestAPI web services for avatre"
		}
		res.json(200,response);
	});

	router.get("/generate/token", function ( req, res, next ) {
		var user = {
			username: "ANAGRAFICA",
			passwd : "ANAGRAFICA_ava3"
		}
		var sign = jwt.sign(user, config.secret);
		res.json({
			success:true,
			token: sign,
			message: "Enjoy it!!!"
		})
	});

	// first router for token verification
	userRouter.set( router );

	// put here all request that has been verified
	fs
	.readdirSync(__dirname)
	.filter(function(file) {
		return (file.indexOf('.') !== 0) && (path.join(__dirname, file) !== basename) && ( file !== "user.js") && (file.slice(-3) === '.js');
	})
	.forEach(function(file) {
		require(path.join(__dirname, file)).set( router );
	});

	
	// // put here all request that has been verified
	// searchRouter.set( router );
	// syncRouter.set( router );
	// tkVerRouter.set( router );
	// avabrowser.set ( router );
	// users.set( router );
	// customers.set( router );
}