var controller 	= require("../controllers/userController");
module.exports.set = function ( router ) {
	router.post("/api/login", controller.login);
	router.put("/api/user/:webuserid/edit", controller.updateWebUser );
}