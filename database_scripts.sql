alter table farmacia.farch_farmacia
add column bd_source character varying,
add column isnarcotic integer,
add column ispsychotropic integer,
add column drug_code character varying( 300 );

alter table spagnatest.farch_farmacia
add column bd_source character varying,
add column isnarcotic integer,
add column ispsychotropic integer,
add column drug_code character varying( 300 );

alter table vr1881.farch_farmacia
add column bd_source character varying,
add column isnarcotic integer,
add column ispsychotropic integer,
add column drug_code character varying( 300 );

alter table flexifarma.farch_farmacia
add column bd_source character varying,
add column isnarcotic integer,
add column ispsychotropic integer,
add column drug_code character varying( 300 );

alter table farmacia.fstorico_ord_tord
add column ftaope_id_reopen integer,
add column date_reopen timestamp;

alter table spagnatest.fstorico_ord_tord
add column ftaope_id_reopen integer,
add column date_reopen timestamp;

alter table vr1881.fstorico_ord_tord
add column ftaope_id_reopen integer,
add column date_reopen timestamp;

alter table flexifarma.fstorico_ord_tord
add column ftaope_id_reopen integer,
add column date_reopen timestamp;

alter table farmacia.ftabella_vendita
add column close_id_park integer;

alter table spagnatest.ftabella_vendita
add column close_id_park integer;

alter table vr1881.ftabella_vendita
add column close_id_park integer;

alter table flexifarma.ftabella_vendita
add column close_id_park integer;

alter table farmacia.farch_farmacia
add column atc_description character varying(120);

alter table spagnatest.farch_farmacia
add column atc_description character varying(120);

alter table vr1881.farch_farmacia
add column atc_description character varying(120);

alter table flexifarma.farch_farmacia
add column atc_description character varying(120);

create table farmacia.ftabella_mancate_vendite (
	id integer not null primary key,
	anapro_id_prodotto integer not null,
	qta integer default 0::integer,
	data_registrazione timestamp without time zone default now(),
	ftaope_id integer,
	farmacia_id integer,
	flg_ann character varying(1) default 'N'::character varying
);

create table spagnatest.ftabella_mancate_vendite (
	id integer not null primary key,
	anapro_id_prodotto integer not null,
	qta integer default 0::integer,
	data_registrazione timestamp without time zone default now(),
	ftaope_id integer,
	farmacia_id integer,
	flg_ann character varying(1) default 'N'::character varying
);

create table vr1881.ftabella_mancate_vendite (
	id integer not null primary key,
	anapro_id_prodotto integer not null,
	qta integer default 0::integer,
	data_registrazione timestamp without time zone default now(),
	ftaope_id integer,
	farmacia_id integer,
	flg_ann character varying(1) default 'N'::character varying
);

create table flexifarma.ftabella_mancate_vendite (
	id integer not null primary key,
	anapro_id_prodotto integer not null,
	qta integer default 0::integer,
	data_registrazione timestamp without time zone default now(),
	ftaope_id integer,
	farmacia_id integer,
	flg_ann character varying(1) default 'N'::character varying
);

create view farmacia.ownproducttype as
	select distinct producttype_id, producttype_code, producttype_description, producttype_reference
	from farmacia.farch_farmacia;

create view spagnatest.ownproducttype as
	select distinct producttype_id, producttype_code, producttype_description, producttype_reference
	from spagnatest.farch_farmacia;

create view vr1881.ownproducttype as
	select distinct producttype_id, producttype_code, producttype_description, producttype_reference
	from vr1881.farch_farmacia;

create view flexifarma.ownproducttype as
	select distinct producttype_id, producttype_code, producttype_description, producttype_reference
	from flexifarma.farch_farmacia;

create view farmacia.ownatc as
	select distinct atc_code, atc_description 
	from farmacia.farch_farmacia;

create view spagnatest.ownatc as
	select distinct atc_code, atc_description 
	from spagnatest.farch_farmacia;

create view vr1881.ownatc as
	select distinct atc_code, atc_description 
	from vr1881.farch_farmacia;

create view flexifarma.ownatc as
	select distinct atc_code, atc_description 
	from flexifarma.farch_farmacia;

INSERT INTO anagrafica.tabella_funzione(ID, CODICE, DESCRIZIONE, FLG_TIPO, FLG_AMBITO, VALORE, FLG_ABILITATA, FLG_PASSWORD, COD_GRUPPO, SEQUENZA, FLG_I18N, CHIAVE_I18N, ID_PADRE, FLG_ANN, IMMAGINE, COLORE) VALUES (867, 'F051', 'i18n:ava3.text.userinfo', 'O', 'F', 'userinformation', 'N', null, null, null, null, null, 8, 'N', null, null);
INSERT INTO anagrafica.tabella_funzione(ID, CODICE, DESCRIZIONE, FLG_TIPO, FLG_AMBITO, VALORE, FLG_ABILITATA, FLG_PASSWORD, COD_GRUPPO, SEQUENZA, FLG_I18N, CHIAVE_I18N, ID_PADRE, FLG_ANN, IMMAGINE, COLORE) VALUES (868, 'F052', 'i18n:ava3.text.categoriePersonalizzate','O','F','categoriePersonali','S',null,null,null,null,null,11,'N',null,null);

alter table credentials.ac_webuser
add column usertoken character varying;

create table farmacia.ftabella_categoria_personalizzata (
	id integer not null primary key,
	descrizione character varying(100),
	codice character varying(30),
	flg_ann character varying(1) default 'N'::character varying,
	farmacia_id integer
);

create table spagnatest.ftabella_categoria_personalizzata (
	id integer not null primary key,
	descrizione character varying(100),
	codice character varying(30),
	flg_ann character varying(1) default 'N'::character varying,
	farmacia_id integer
);

create table vr1881.ftabella_categoria_personalizzata (
	id integer not null primary key,
	descrizione character varying(100),
	codice character varying(30),
	flg_ann character varying(1) default 'N'::character varying,
	farmacia_id integer
);

create table flexifarma.ftabella_categoria_personalizzata (
	id integer not null primary key,
	descrizione character varying(100),
	codice character varying(30),
	flg_ann character varying(1) default 'N'::character varying,
	farmacia_id integer
);

alter table farmacia.farch_farmacia
drop column categoria_personalizzata,
add column ftacaper_id integer;

alter table spagnatest.farch_farmacia
drop column categoria_personalizzata,
add column ftacaper_id integer;

alter table vr1881.farch_farmacia
drop column categoria_personalizzata,
add column ftacaper_id integer;

alter table flexifarma.farch_farmacia
drop column categoria_personalizzata,
add column ftacaper_id integer;

create table vr1881.preferences (
	id integer not null primary key,
	code character varying(10),
	description character varying(100),
	flgboss integer default 0,
	flgadmin integer default 0,
	flggroup integer default 0,
	defaultvalue character varying(100),
	formname character varying(200),
	enables integer default 0,
	parent_id integer
);

create table vr1881.movementtypes (
id integer not null primary key,
code character varying(5),
description character varying(100),
movestock integer,
ordersign integer,
statesale integer,
statbrought integer,
statrefound integer,
statreserved integer,
statlostsale integer,
flg_ann character varying(1) default 'N'::character varying
);

alter table farmacia.fstorico_ricetta_dett
add column anapro_id_prodotto integer,
add column fstfof_id integer,
add column positioninsheet integer,
add column dispatchid integer;

alter table farmacia.fstorico_ricetta
add column dispatchid bigint;

alter table vr1881.fstorico_ricetta_dett
add column anapro_id_prodotto integer,
add column fstfof_id integer,
add column positioninsheet integer,
add column dispatchid integer;

alter table vr1881.fstorico_ricetta
add column dispatchid bigint;

alter table flexifarma.fstorico_ricetta_dett
add column anapro_id_prodotto integer,
add column fstfof_id integer,
add column positioninsheet integer,
add column dispatchid integer;

alter table flexifarma.fstorico_ricetta
add column dispatchid bigint;

alter table spagnatest.fstorico_ricetta_dett
add column anapro_id_prodotto integer,
add column fstfof_id integer,
add column positioninsheet integer,
add column dispatchid integer;

alter table spagnatest.fstorico_ricetta
add column dispatchid bigint;

alter table farmacia.ftabella_vendita
add column reserved_id integer,
add column credit_id integer;

alter table vr1881.ftabella_vendita
add column reserved_id integer,
add column credit_id integer;

alter table flexifarma.ftabella_vendita
add column reserved_id integer,
add column credit_id integer;

alter table spagnatest.ftabella_vendita
add column reserved_id integer,
add column credit_id integer;

alter table farmacia.ftabella_vendita
alter column saletype type character varying(30);

alter table vr1881.ftabella_vendita
alter column saletype type character varying(30);

alter table spagnatest.ftabella_vendita
alter column saletype type character varying(30);

alter table flexifarma.ftabella_vendita
alter column saletype type character varying(30);

alter table farmacia.fstorico_cel_buono
add column centro_costo character varying(20);

alter table farmacia.fstorico_cel_buono
add column distretto character varying(100);

alter table farmacia.fstorico_cel_buono
add column conto_economico character varying(20);

alter table farmacia.fstorico_forn_bolla
add column valore_doc_netto numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column valore_doc_lordo numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column valore_caricato_netto numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column valore_caricato_lordo numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column qta_doc numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column qta_caricato numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column valore_listino numeric(15,5);

alter table farmacia.fstorico_forn_bolla
add column valore_listino_caricato numeric;

alter table farmacia.fstorico_ricetta_dett
add column nota_aifa character varying(3);

alter table farmacia.ftabella_param_re
add column userid_730 character varying(30);

alter table farmacia.ftabella_param_re
add column password_730 character varying(30);

alter table farmacia.ftabella_param_re
add column pincode_730 character varying(30);

alter table farmacia.ftabella_param_re
add column ftaope_id_730 integer;

alter table farmacia.ftabella_param_re
add column flg_frequenza_730 character varying(1) DEFAULT 'N'::character varying;

alter table farmacia.ftabella_param_re
add column tipo_gg_730 double precision;

alter table farmacia.ftabella_param_re
add column flg_tipo_invio_730 character varying(2) DEFAULT 'N'::character varying;

alter table farmacia.ftabella_param_re
add column email_tipo_invio_730 character varying(100);

alter table farmacia.ftabella_param_re
add column flg_tipo_notifica_730 character varying(1);

alter table farmacia.ftabella_vendita
add column product_type_code character varying(4);

alter table farmacia.ftabella_vendita
add column sale_type_code character varying(4);

alter table vr1881.fstorico_cel_buono
add column centro_costo character varying(20);

alter table vr1881.fstorico_cel_buono
add column distretto character varying(100);

alter table vr1881.fstorico_cel_buono
add column conto_economico character varying(20);

alter table vr1881.fstorico_forn_bolla
add column valore_doc_netto numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column valore_doc_lordo numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column valore_caricato_netto numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column valore_caricato_lordo numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column qta_doc numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column qta_caricato numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column valore_listino numeric(15,5);

alter table vr1881.fstorico_forn_bolla
add column valore_listino_caricato numeric;

alter table vr1881.fstorico_ricetta_dett
add column nota_aifa character varying(3);

alter table vr1881.ftabella_param_re
add column userid_730 character varying(30);

alter table vr1881.ftabella_param_re
add column password_730 character varying(30);

alter table vr1881.ftabella_param_re
add column pincode_730 character varying(30);

alter table vr1881.ftabella_param_re
add column ftaope_id_730 integer;

alter table vr1881.ftabella_param_re
add column flg_frequenza_730 character varying(1) DEFAULT 'N'::character varying;

alter table vr1881.ftabella_param_re
add column tipo_gg_730 double precision;

alter table vr1881.ftabella_param_re
add column flg_tipo_invio_730 character varying(2) DEFAULT 'N'::character varying;

alter table vr1881.ftabella_param_re
add column email_tipo_invio_730 character varying(100);

alter table vr1881.ftabella_param_re
add column flg_tipo_notifica_730 character varying(1);

alter table vr1881.ftabella_vendita
add column product_type_code character varying(4);

alter table vr1881.ftabella_vendita
add column sale_type_code character varying(4);

alter table spagnatest.fstorico_cel_buono
add column centro_costo character varying(20);

alter table spagnatest.fstorico_cel_buono
add column distretto character varying(100);

alter table spagnatest.fstorico_cel_buono
add column conto_economico character varying(20);

alter table spagnatest.fstorico_forn_bolla
add column valore_doc_netto numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column valore_doc_lordo numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column valore_caricato_netto numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column valore_caricato_lordo numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column qta_doc numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column qta_caricato numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column valore_listino numeric(15,5);

alter table spagnatest.fstorico_forn_bolla
add column valore_listino_caricato numeric;

alter table spagnatest.fstorico_ricetta_dett
add column nota_aifa character varying(3);

alter table spagnatest.ftabella_param_re
add column userid_730 character varying(30);

alter table spagnatest.ftabella_param_re
add column password_730 character varying(30);

alter table spagnatest.ftabella_param_re
add column pincode_730 character varying(30);

alter table spagnatest.ftabella_param_re
add column ftaope_id_730 integer;

alter table spagnatest.ftabella_param_re
add column flg_frequenza_730 character varying(1) DEFAULT 'N'::character varying;

alter table spagnatest.ftabella_param_re
add column tipo_gg_730 double precision;

alter table spagnatest.ftabella_param_re
add column flg_tipo_invio_730 character varying(2) DEFAULT 'N'::character varying;

alter table spagnatest.ftabella_param_re
add column email_tipo_invio_730 character varying(100);

alter table spagnatest.ftabella_param_re
add column flg_tipo_notifica_730 character varying(1);

alter table spagnatest.ftabella_vendita
add column product_type_code character varying(4);

alter table spagnatest.ftabella_vendita
add column sale_type_code character varying(4);

alter table flexifarma.fstorico_cel_buono
add column centro_costo character varying(20);

alter table flexifarma.fstorico_cel_buono
add column distretto character varying(100);

alter table flexifarma.fstorico_cel_buono
add column conto_economico character varying(20);

alter table flexifarma.fstorico_forn_bolla
add column valore_doc_netto numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column valore_doc_lordo numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column valore_caricato_netto numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column valore_caricato_lordo numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column qta_doc numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column qta_caricato numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column valore_listino numeric(15,5);

alter table flexifarma.fstorico_forn_bolla
add column valore_listino_caricato numeric;

alter table flexifarma.fstorico_ricetta_dett
add column nota_aifa character varying(3);

alter table flexifarma.ftabella_param_re
add column userid_730 character varying(30);

alter table flexifarma.ftabella_param_re
add column password_730 character varying(30);

alter table flexifarma.ftabella_param_re
add column pincode_730 character varying(30);

alter table flexifarma.ftabella_param_re
add column ftaope_id_730 integer;

alter table flexifarma.ftabella_param_re
add column flg_frequenza_730 character varying(1) DEFAULT 'N'::character varying;

alter table flexifarma.ftabella_param_re
add column tipo_gg_730 double precision;

alter table flexifarma.ftabella_param_re
add column flg_tipo_invio_730 character varying(2) DEFAULT 'N'::character varying;

alter table flexifarma.ftabella_param_re
add column email_tipo_invio_730 character varying(100);

alter table flexifarma.ftabella_param_re
add column flg_tipo_notifica_730 character varying(1);

alter table flexifarma.ftabella_vendita
add column product_type_code character varying(4);

alter table flexifarma.ftabella_vendita
add column sale_type_code character varying(4);

alter table vr1881.fstorico_documento
alter column numero_colli type integer;

alter table vr1881.fstorico_documento
alter column protocollo type integer;

alter table vr1881.fstorico_documento
alter column cod_univoco_xml type integer;

alter table farmacia.fstorico_documento
alter column numero_colli type integer;

alter table farmacia.fstorico_documento
alter column protocollo type integer;

alter table farmacia.fstorico_documento
alter column cod_univoco_xml type integer;

alter table flaxifarma.fstorico_documento
alter column numero_colli type integer;

alter table flaxifarma.fstorico_documento
alter column protocollo type integer;

alter table flaxifarma.fstorico_documento
alter column cod_univoco_xml type integer;

alter table spagnatest.fstorico_documento
alter column numero_colli type integer;

alter table spagnatest.fstorico_documento
alter column protocollo type integer;

alter table spagnatest.fstorico_documento
alter column cod_univoco_xml type integer;


alter table farmacia.ftabella_postazione_perif
add column type character varying(20),
add column parameters character varying,
add column protocol character varying(20);

alter table flexifarma.ftabella_postazione_perif
add column type character varying(20),
add column parameters character varying,
add column protocol character varying(20);

alter table spagnatest.ftabella_postazione_perif
add column type character varying(20),
add column parameters character varying,
add column protocol character varying(20);

alter table vr1881.ftabella_postazione_perif
add column type character varying(20),
add column parameters character varying,
add column protocol character varying(20);

alter table farmacia.ftabella_vendita
	add column dispensationid bigint;

alter table vr1881.ftabella_vendita
	add column dispensationid bigint;

alter table flexifarma.ftabella_vendita
	add column dispensationid bigint;

alter table spagnatest.ftabella_vendita
	add column dispensationid bigint;

alter table farmacia.ftabella_param_tr_ord
add column farmacia_id integer;

alter table flexifarma.ftabella_param_tr_ord
add column farmacia_id integer;

alter table spagnatest.ftabella_param_tr_ord
add column farmacia_id integer;

alter table vr1881.ftabella_param_tr_ord
add column farmacia_id integer;

alter table farmacia.fstorico_scontrino
add column progressivo integer,
add column progressivo_unico integer;

alter table vr1881.fstorico_scontrino
add column progressivo integer,
add column progressivo_unico integer;

alter table flaxifarma.fstorico_scontrino
add column progressivo integer,
add column progressivo_unico integer;

alter table spagnatest.fstorico_scontrino
add column progressivo integer,
add column progressivo_unico integer;

alter table vr1881.farch_farmacia
add column nomenclature character varying(13),
add column hasmilk character varying(2),
add column hasgluten character varying(2);

alter table farmacia.ftabella_operatore
add column colore character varying(10);

alter table vr1881.ftabella_operatore
add column colore character varying(10);

alter table flexifarma.ftabella_operatore
add column colore character varying(10);

alter table spagnatest.ftabella_operatore
add column colore character varying(10);

alter table farmacia.fstorico_sospesiprenotati_dett
add column anapro_id_prodotto integer,
add column qta integer,
add column salePrice numeric(15,5) default 0;

alter table vr1881.fstorico_sospesiprenotati_dett
add column anapro_id_prodotto integer,
add column qta integer,
add column salePrice numeric(15,5) default 0;

alter table flexifarma.fstorico_sospesiprenotati_dett
add column anapro_id_prodotto integer,
add column qta integer,
add column salePrice numeric(15,5) default 0;

alter table spagnatest.fstorico_sospesiprenotati_dett
add column anapro_id_prodotto integer,
add column qta integer,
add column salePrice numeric(15,5) default 0;

alter table farmacia.fstorico_request_re
add column obj_req6 character varying,
add column obj_req7 character varying,
add column obj_req8 character varying,
add column obj_req9 character varying,
add column obj_req10 character varying,
add column obj_req11 character varying,
add column obj_req12 character varying,
add column obj_res9 character varying,
add column obj_res10 character varying,
add column obj_res11 character varying,
add column obj_res12 character varying;

ALTER TABLE farmacia.fstorico_request_re
ALTER COLUMN flg_tipo_richiesta TYPE character varying;

ALTER TABLE spagnatest.farch_giacenza
	DROP CONSTRAINT spagnatest_farch_giacenza_anapro_id_prodotto_fkey;

ALTER TABLE spagnatest.fstorico_movimento
	DROP CONSTRAINT spagnatest_fstorico_movimento_anapro_id_prodotto_fkey;

ALTER TABLE spagnatest.fstorico_ord_dett
	DROP CONSTRAINT spagnatest_fstorico_ord_dett_anapro_id_prodotto_fkey;

create type anagrafica.productdetails as (
	primarykey integer,
	bcr character varying(9),
	ean character varying(13),
	emea character varying(20),
	internalcode character varying(13),
	description character varying,
	pricebd numeric(11,5),
	company character varying,
	companyid integer,
	companycode character varying,
	composition character varying,
	compositionid integer,
	compositioncode character varying,
	vat numeric(6,3),
	vatid integer,
	monograph character varying,
	atc character varying,
	atcdescription character varying,
	atctype character varying,
	ptypeid integer,
	ptype character varying(60),
	ptypecode character varying,,
	ptypereference character varying(1),
	pclass character varying(3),
	incommerce character varying(1),
	pricetype character varying(60),
	replacedby character varying(9),
	replaces character varying(9),
	productform character varying(60),
	prescriptiontype character varying(60),
	creationdate timestamp,
	startcommerce timestamp,
	gluten character varying(1),
	milk character varying(1),
	imagelink character varying,
	nomenclature character varying(13),
	notsalabledate date,
	notsalablecode character varying(2),
	notsalabletype character varying(2),
	eq_list character varying(10),
	eq_eq1 character varying(10),
	eq_eq2 character varying(10)
);

alter table farmacia.fstorico_request_re
add column obj_req6 character varying,
add column obj_req7 character varying,
add column obj_req8 character varying,
add column obj_req9 character varying,
add column obj_req10 character varying,
add column obj_req11 character varying,
add column obj_req12 character varying,
add column obj_res9 character varying,
add column obj_res10 character varying,
add column obj_res11 character varying,
add column obj_res12 character varying;

alter table vr1881.fstorico_request_re
add column obj_req6 character varying,
add column obj_req7 character varying,
add column obj_req8 character varying,
add column obj_req9 character varying,
add column obj_req10 character varying,
add column obj_req11 character varying,
add column obj_req12 character varying,
add column obj_res9 character varying,
add column obj_res10 character varying,
add column obj_res11 character varying,
add column obj_res12 character varying;

alter table spagnatest.fstorico_request_re
add column obj_req6 character varying,
add column obj_req7 character varying,
add column obj_req8 character varying,
add column obj_req9 character varying,
add column obj_req10 character varying,
add column obj_req11 character varying,
add column obj_req12 character varying,
add column obj_res9 character varying,
add column obj_res10 character varying,
add column obj_res11 character varying,
add column obj_res12 character varying;

alter table flexifarma.fstorico_request_re
add column obj_req6 character varying,
add column obj_req7 character varying,
add column obj_req8 character varying,
add column obj_req9 character varying,
add column obj_req10 character varying,
add column obj_req11 character varying,
add column obj_req12 character varying,
add column obj_res9 character varying,
add column obj_res10 character varying,
add column obj_res11 character varying,
add column obj_res12 character varying;

ALTER TABLE farmacia.fstorico_request_re
ALTER COLUMN flg_tipo_richiesta TYPE character varying;

ALTER TABLE spagnatest.fstorico_request_re
ALTER COLUMN flg_tipo_richiesta TYPE character varying;

ALTER TABLE flexifarma.fstorico_request_re
ALTER COLUMN flg_tipo_richiesta TYPE character varying;

ALTER TABLE vr1881.fstorico_request_re
ALTER COLUMN flg_tipo_richiesta TYPE character varying;

alter table farmacia.ftabella_vendita
add column farmacia_id integer,
add column stockbefore integer,
add column stockafter integer,
add column islocked integer DEFAULT 0,
add column canceled integer DEFAULT 0;

alter table spagnatest.ftabella_vendita
add column farmacia_id integer,
add column stockbefore integer,
add column stockafter integer,
add column islocked integer DEFAULT 0,
add column canceled integer DEFAULT 0;

alter table flexifarma.ftabella_vendita
add column farmacia_id integer,
add column stockbefore integer,
add column stockafter integer,
add column islocked integer DEFAULT 0,
add column canceled integer DEFAULT 0;

alter table vr1881.ftabella_vendita
add column farmacia_id integer,
add column stockbefore integer,
add column stockafter integer,
add column islocked integer DEFAULT 0,
add column canceled integer DEFAULT 0;

alter table farmacia.fstorico_foglio_fustelli
add column ftapos_id double precision,
add column ftaope_id double precision,
add column farmacia_id double precision;

alter table spagnatest.fstorico_foglio_fustelli
add column ftapos_id double precision,
add column ftaope_id double precision,
add column farmacia_id double precision;

alter table flexifarma.fstorico_foglio_fustelli
add column ftapos_id double precision,
add column ftaope_id double precision,
add column farmacia_id double precision;

alter table vr1881.fstorico_foglio_fustelli
add column ftapos_id double precision,
add column ftaope_id double precision,
add column farmacia_id double precision;


create or replace function anagrafica.getproduct ( primarykey integer )
returns anagrafica.productdetails
as
$$
declare
	p_rec anagrafica.productdetails;
	ptypeid integer;
	compositionid integer;
	atcid integer;
	companyid integer;
begin

	select 	id_prodotto, cod_minsan, descrizione, cod_sost_il, cod_sost_da,
			cod_emea, cod_int_ditta, anapri_id, data_prima_reg,data_inzio_comm,
			tabtipr_id, anadit_id, flg_commercio, anaatgm_id, flg_glutine,
			flg_lattosio, cod_monografia, cod_cartella, estensione_imm,
			cod_nome_tar
	into 	p_rec.primarykey, p_rec.bcr, p_rec.description, p_rec.replaces,
			p_rec.replacedby, p_rec.emea, p_rec.internalcode, p_rec.compositionid,
			p_rec.creationdate, p_rec.startcommerce, p_rec.ptypeid, p_rec.companyid,
			p_rec.incommerce, atcid, p_rec.gluten, p_rec.milk,
			p_rec.nomenclature
	from anagrafica.anag_prodotti
	where id_prodotto = primarykey;

	p_rec.ean 			= anagrafica.productean(p_rec.primarykey);
	p_rec.pclass 		= anagrafica.productpclass(p_rec.primarykey);
	p_rec.pricebd 		= anagrafica.productpricebd(p_rec.primarykey);
	p_rec.vat 			= anagrafica.productvat(p_rec.primarykey);

	select ragione_sociale, codice
	into p_rec.company, p_rec.companycode
	from anagrafica.anag_ditta where id = p_rec.companyid;

	select descrizione_sint, codice
	into p_rec.ptype, p_rec.ptypecode
	from anagrafica.tabella_tipo_prodotto where id = p_rec.ptypeid;

	select codice, descrizione, flg_genere
	into p_rec.atc, p_rec.atcdescription, p_rec.atctype
	from anagrafica.anag_atc_gmp where id = atcid;

	select codice, descrizione
	into p_rec.compositioncode, p_rec.composition
	from anagrafica.anag_principio where id = p_rec.compositionid;

	return p_rec;
end
$$ language plpgsql;
/*            FUNCTION       */
create or replace function anagrafica.getleafletsez(primarykey integer)
return character varying
as
$$
declare
	section character varying;
begin
	select descrizione into section from anagrafica.tabella_monografia_sez where id = primarykey and flg_ann <> 'S';
	return section;
end
$$ language plpgsql;
/*            FUNCTION       */
create or replace function anagrafica.getleaflet ( primarycode integer )
returns character varying
as
$$
declare
	monographs character varying;
	monograph cursor for select * from anagrafica.tabella_monografia_fog where codice = primarycode and flg_ann <> 'S' order by tabmose_id, progressivo;
	r anagrafica.tabella_monografia_fog%rowtype;
begin

	for mono in monograph loop



	end loop;

	return monographs;
end
$$ language plpgsql;
/*            FUNCTION       */
create or replace procedure vr1881.syncproducts(primarykey integer)
as
$$
declare
	products cursor for select anapro_id_prodotto from vr1881.farch_farmacia order by id asc;
	productinfo anagrafica.productdetails%rowtype;
begin
	for p in products loop
		productinfo = anagrafica.getproduct(p.anapro_id_prodotto);
		raise notice productinfo.bcr_minsan||" "||productinfo.description;
	end loop;
end
$$ language plpgsql;
