var pg = require( "../libs/query");
var _ = require( "underscore" );
var config 	= require("../config");

module.exports.getMovementsListForSync = function ( req, res, next ) {
	var sql = "select * from anagrafica.tabella_tipo_movimento order by id asc";
	pg.query( sql, null, function ( err, queryResult ) {
		var result = {};
		var rows = queryResult.rows;

		if ( !rows ) {
			result.error 	= '00001';
			result.message 	= "Error getting movement types";
			result.data 	= null;
			return res.json( 200, result );
		}

		if ( rows.length == 0 ) {
			result.error 	= '00002';
			result.message 	= "No movement types found in table";
			result.data 	= null;
			return res.json( 200, result );
		}

		result.data 	= rows;

		return res.json( result );
	});
}