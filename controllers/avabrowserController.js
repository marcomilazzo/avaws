var pg 	= require("../libs/query");
var _ 	= require("underscore");

module.exports.getCommand = function ( req, res, next ) {
	var iso = req.params.iso
	var sql = "select * from usersettings.ac_abcommand where isonation = $1";
	pg.cnQuery( sql, [iso], function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			result.success = false;
			result.error = '00003';
			result.message = err;
			return res.json(500, result);
		}

		var rows = queryResult.rows;

		if ( !rows ) {
			result.success 	= false;
			result.error 	= '00001';
			result.message 	= "User not found";
			return res.json( 200, result );
		}

		return res.json(200, rows);
	});
};

module.exports.getProperties = function ( req, res, next ) {
	var iso 	= req.params.iso
	var scope 	= req.params.scope;
	var sql 	= "select * from usersettings.ac_abresultmask where scope = $1 and isonation = $2";
	var rows 	= {
		fields:null,
		relations:null
	};
	var row = null;
	pg.cnQuery( sql, [scope, iso], function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			result.success = false;
			result.error = '00003';
			result.message = err;
			return res.json(500, result);
		}

		rows.fields = queryResult.rows[0];

		if ( rows.fields && rows.fields.fields ) {
			rows.fields.fields = JSON.parse(rows.fields.fields);
		}

		sql = "select * from usersettings.ac_abrelations where scope = $1 and isonation = $2";
		pg.cnQuery( sql, [scope, iso], function ( err, queryResult ) {
			if ( err ) {
				result.success = false;
				result.error = '00003';
				result.message = err;
				return res.json(500, result);
			}

			rows.relations = queryResult.rows[0];

			if ( rows.relations && rows.relations.relations ) {
				rows.relations.relations = JSON.parse(rows.relations.relations);
			}

			return res.json(200, rows);

		});
	});
};

module.exports.getResultMask = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var sql 		= "select * from usersettings.ac_abresultmask where isonation = $1 ";
	var queryParams = [iso];
	if ( primary ) {
		sql += " and ac_abresultmask_id = $2";
		queryParams.push( primary );
	}
	var rows = null;
	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = null;
		if ( err ) {
			result = [];
			return res.json(500, result);
		}

		rows = queryResult.rows;

		_.map( rows, function ( result ) { result.fields = JSON.parse( result.fields ); return result; } );

		return res.json( 200, rows );

	});
};

module.exports.getSidebar = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var sql 		= "select * from usersettings.ac_abrelations where isonation = $1 ";
	var queryParams = [iso];
	if ( primary ) {
		sql += " and ac_abrelations_id = $2";
		queryParams.push( primary );
	}
	var rows = null;
	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = null;
		if ( err ) {
			result = [];
			return res.json(500, result);
		}

		rows = queryResult.rows;

		_.map( rows, function ( result ) { result.relations = JSON.parse( result.relations ); return result; } );

		return res.json( 200, rows );

	});
};

module.exports.updateResultMask = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var params 		= req.body;
	var sql 		= "UPDATE usersettings.ac_abresultmask set ";
	var set 		= [];
	var queryParams = [];
	set.push( " scope = $1" );
	queryParams.push( params.scope );
	set.push( " fields = $2" );
	queryParams.push( JSON.stringify( params.fields ) );
	set.push( " isonation = $3 " );
	queryParams.push( iso );
	var where = " where ac_abresultmask_id = " + primary;

	sql = sql + set.join(",") + where;

	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			return res.json( 500, result );
		}
		return res.json( 200, queryResult );
	});
};

module.exports.updateSidebar = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var params 		= req.body;
	var sql 		= "UPDATE usersettings.ac_abrelations set ";
	var set 		= [];
	var queryParams = [];
	console.log( req.body );
	set.push( " scope = $1" );
	queryParams.push( params.scope );
	set.push( " relations = $2" );
	queryParams.push( JSON.stringify( params.relations ) );
	set.push( " isonation = $3 " );
	queryParams.push( iso );
	var where = " where ac_abrelations_id = " + primary;

	sql = sql + set.join(",") + where;

	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			return res.json( 500, result );
		}
		return res.json( 200, queryResult );
	});
};

module.exports.storeResultMask = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var params 		= req.body;
	var sql 		= "insert into usersettings.ac_abresultmask ";
	var set 		= [];
	var queryParams = [];
	set.push( " scope " );
	queryParams.push( params.scope );
	set.push( " relations " );
	queryParams.push( JSON.stringify( params.relations ) );
	set.push( " isonation " );
	queryParams.push( iso );
	var values = "(" + new Array(queryParams.length).map( function ( e, index ) { return "$" + ( index + 1 ) } ).join(",") + ")";

	sql = sql + "(" + set.join(",") + ") values" + values;

	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			return res.json( 500, result );
		}
		return res.json( 200, queryResult );
	});
};

module.exports.storeSidebar = function ( req, res, next ) {
	var iso 		= req.params.iso
	var primary 	= req.params.primary;
	var params 		= req.body;
	var sql 		= "insert into usersettings.ac_abresultmask ";
	var set 		= [];
	var queryParams = [];
	set.push( " scope " );
	queryParams.push( params.scope );
	set.push( " relations " );
	queryParams.push( JSON.stringify( params.relations ) );
	set.push( " isonation " );
	queryParams.push( iso );
	var values = "(" + new Array(queryParams.length).map( function ( e, index ) { return "$" + ( index + 1 ) } ).join(",") + ")";

	sql = sql + "(" + set.join(",") + ") values" + values;

	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			return res.json( 500, result );
		}
		return res.json( 200, queryResult );
	});
};
