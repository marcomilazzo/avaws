var token = require("../libs/token");

module.exports.verifyTokenIdentity = function ( req, res, next ) {
	// after middleware check of the token identity return the decoded token as json

	var tk = req.body.token;

	return token.verifyToken( tk, function ( err, result ) {
		if ( err ) {
			return res.json(200, err);
		}
		return res.json(200, result);
	});
}