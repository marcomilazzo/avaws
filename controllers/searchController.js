var pg 	= require("../libs/query");
var _ 	= require("underscore");

module.exports.srcIndex = function ( req, res, next ) {
	var response = {
		title: "search",
		description: "Search Engine for avatre"
	}
	res.json(response);
}

function searchProduct ( parameters, callback) {
	var sql = "select ap.id_prodotto, ap.cod_minsan, ap.descrizione, ap.anapri_id, ap.anadit_id, ";
	sql += " te.codice, ap.flg_commercio, ad.ragione_sociale, ad.codice as companycode, apr.codice as compositioncode, ";
	sql += " apr.descrizione as composition, anagrafica.productatc(ap.id_prodotto) as atc, ";
	sql += " anagrafica.productpclass(ap.id_prodotto) as pclass, ";
	sql += " anagrafica.productvat(ap.id_prodotto) as vat, ";
	sql += " anagrafica.productpricebd(ap.id_prodotto) as bdprice ";
	sql += " from anagrafica.anag_prodotti ap left join anagrafica.tabella_ean te on ( ap.id_prodotto = te.anapro_id_prodotto ) left join anagrafica.anag_ditta ad on ( ap.anadit_id = ad.id ) left join anagrafica.anag_principio apr on ( ap.anapri_id = apr.id ) where ";

	var params = [];

	if ( !( _.isUndefined( parameters.searchterm ) ) ) {
		// add search term 
		params.push( parameters.searchterm );
		// create where clause
		if ( /^(\d+)$/.test( req.params.searchterm ) ) {
			if ( parameters.searchterm.length == 13 ) {
				sql += " te.codice = $" + ( params.length );
			} else {
				sql += " ap.cod_minsan = $" + ( params.length );
			}
		} else {
			parameters.searchterm = parameters.searchterm.replace(/\ /g, "%");
			sql += " ap.descrizione like $" + ( params.length );
		}
	}

	// composition
	if ( !( _.isUndefined( parameters.company ) ) ) {
		// add search term 
		params.push( parameters.company );
		sql += " ap.anadit_id = $" + ( params.length ) + " and ";
	}
	if ( !( _.isUndefined( parameters.composition ) ) ) {
		// add search term 
		params.push( parameters.composition );
		sql += " ap.anapri_id = $" + ( params.length ) + " and ";
	}

	pg.query( sql, params, function ( err, queryResult ) {
		if ( err ) {
			return callback( err, null );
		}
		var rows = queryResult.rows;

		return callback( null, rows );

	})
}

function counterRelations ( parameters, callback ) {

	var params = new Object(parameters);
	var select = "select * from ";
	switch ( params.scopes ) {
		case "product": {
			select.concat( " productSidebar( $1 );" );
			break;
		}
	}
	pg.query( select, [params.primary], function ( err, queryResult ) {
		if ( err ) {
			return callback( err, null );
		}
		var row 	= queryResult.rows[0];

		return callback( null, result );
	});
}

module.exports.srcSearchProduct = function ( req, res, next ) {
	
	var sql = "select ap.id_prodotto, ap.cod_minsan, ap.descrizione, ap.anapri_id, ap.anadit_id, ";
	sql += " te.codice, ap.flg_commercio, ad.ragione_sociale, ad.codice as companycode, apr.codice as compositioncode, ";
	sql += " apr.descrizione as composition, anagrafica.productatc(ap.id_prodotto) as atc, ";
	sql += " anagrafica.productpclass(ap.id_prodotto) as pclass, ";
	sql += " anagrafica.productvat(ap.id_prodotto) as vat, ";
	sql += " anagrafica.productpricebd(ap.id_prodotto) as bdprice ";
	sql += " from anagrafica.anag_prodotti ap left join anagrafica.tabella_ean te on ( ap.id_prodotto = te.anapro_id_prodotto ) left join anagrafica.anag_ditta ad on ( ap.anadit_id = ad.id ) left join anagrafica.anag_principio apr on ( ap.anapri_id = apr.id ) where ";
	if ( /^(\d+)$/.test(req.params.searchterm) ) {
		if ( req.params.searchterm.length == 13 ) {
			sql += " te.codice = $1";
		} else {
			sql += " ap.cod_minsan = $1";
		}
		pg.query( sql, [req.params.searchterm], function ( err, result ) {
			var products = [];
			if ( _.isArray(result.rows) ) {
				_.each(result.rows, function ( element, index, key ) {
					products.push({primary: element.id_prodotto, bcr: element.cod_minsan, ean: element.codice, description: element.descrizione, company: {primary: element.anadit_id, code: element.companycode, name: element.ragione_sociale}, composition: {primary:element.anapri_id, code: element.compositioncode, name: element.composition}, atc: element.atc, pclass: element.pclass, vat: element.vat, bdprice: element.bdprice});
				})
			}
			return res.json(200, products);
		});
	} else {
		sql += " ap.descrizione like $1";
		req.params.searchterm = req.params.searchterm.replace(/\ /g, "%");
		pg.query( sql, [req.params.searchterm.toUpperCase() + "%"], function ( err, result ) {
			var products = [];
			if ( _.isArray(result.rows) ) {
				_.each(result.rows, function ( element, index, key ) {
					products.push({primary: element.id_prodotto, bcr: element.cod_minsan, ean: element.codice, description: element.descrizione, company: {primary: element.anadit_id, code: element.companycode, name: element.ragione_sociale}, composition: {primary:element.anapri_id, code: element.compositioncode, name: element.composition}, atc: element.atc, pclass: element.pclass, vat: element.vat, bdprice: element.bdprice});
				})
			}
			return res.json(200, products);
		});
	}
}

module.exports.srcSearchProductsDetails = function ( req, res, next ) {
	var product = req.params.product;
	var sql = "select * from anagrafica.getproduct(" + product + "::integer)";
	pg.query( sql, null, function ( err, result ) {
		if ( err ) {
			console.log(err);
			return res.json( 200, err );
		}
		return res.json( 200, result.rows );
	});
}

/**
 * @description advanced search function to get product for company, composition, etc... this function is called by a post request
 */
module.exports.srcSearchProductAdvanced = function ( req, res, next ) {
	var queryparams = req.body;
	/**
	 * @type {String}
	 */
	var sql = "select ap.id_prodotto, ap.cod_minsan, ap.descrizione, ap.anapri_id, ";
	sql += " ap.anadit_id, te.codice, ap.flg_commercio, ad.ragione_sociale, ";
	sql += " ad.codice as companycode, apr.codice as compositioncode, ";
	sql += " apr.descrizione as composition, anagrafica.productatc(ap.id_prodotto) as atc,";
	sql += " anagrafica.productpclass(ap.id_prodotto) as pclass, ";
	sql += " anagrafica.productvat(ap.id_prodotto) as vat, ";
	sql += " anagrafica.productpricebd(ap.id_prodotto) as bdprice ";
	sql += " from anagrafica.anag_prodotti ap left join anagrafica.tabella_ean te ";
	sql += " on ( ap.id_prodotto = te.anapro_id_prodotto ) left join anagrafica.anag_ditta ad on ";
	sql += "( ap.anadit_id = ad.id ) left join anagrafica.anag_principio apr on ( ap.anapri_id = apr.id ) where ";
	if ( !_.isUndefined( queryparams.company ) ) {
		sql += " ap.anadit_id = " + queryparams.company + " and ";
	}
	if ( !_.isUndefined( queryparams.composition ) ) {
		sql += " ap.anapri_id = " + queryparams.composition + " and ";
	}
	sql = sql.slice(0, -4);
	pg.query( sql, null, function ( err, result ) {
		var products = [];
		if ( err ) {
			console.log(err);
		}
		if ( _.isArray(result.rows) ) {
			_.each(result.rows, function ( element, index, key ) {
				products.push({primary: element.id_prodotto, bcr: element.cod_minsan, ean: element.codice, description: element.descrizione, company: {primary: element.anadit_id, code: element.companycode, name: element.ragione_sociale}, composition: {primary:element.anapri_id, code: element.compositioncode, name: element.composition}, atc: element.atc, pclass: element.pclass, vat: element.vat, bdprice: element.bdprice });
			})
		}
		return res.json(200, products);
	});
}

/**
 * @description obtain the related relation for the sidebar
 */
module.exports.srcProductSidebar = function ( req, res, next ) {
	counterRelations( req.params, function ( err, counters ) {
		if ( err ) {
			return res.json([]);
		}
		var relations = [];
		if ( counters.company > 0 ) {
			relations.push( { "scope": "product", "context": "", "primary": counter.companyid, "description": "i18n:ava3.text.stessaDitta" } );
		}

		if ( counters.composition > 0 ) {
			relations.push( { "scope": "product", "context": "", "primary": conter.compositionid, "description": "i18n:ava3.text.stessoPrincipio" } );
		}

		return res.json( 200, relations );

	});
}

/**
 * @description obtain the related relation for the sidebar
 */
module.exports.srcSameProduct = function ( req, res, next ) {
	
	var parameters = {};

	if ( req.params.scope == "company" ) {
		parameters.company = req.params.primary;
	}
	if ( req.params.scope == "composition" ) {
		parameters.composition = req.params.primary;
	}

	searchProduct( parameters, function ( err, queryResult ) {
		if ( err ) {
			return res.json( 200, [] )
		}
		var products = [];
		if ( _.isArray( queryResult ) ) {
			_.each( queryResult, function ( element, index, key ) {
				products.push({primary: element.id_prodotto, bcr: element.cod_minsan, ean: element.codice, description: element.descrizione, company: {primary: element.anadit_id, code: element.companycode, name: element.ragione_sociale}, composition: {primary:element.anapri_id, code: element.compositioncode, name: element.composition}, atc: element.atc, pclass: element.pclass, vat: element.vat, bdprice: element.bdprice});
			})
		}
		return res.json( 200, products );
	})
}
