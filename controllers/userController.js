var pg 		= require("../libs/query");
var token 	= require("../libs/token");
var config 	= require("../config");
var crypto 	= require("crypto");
var _ 		= require("underscore");

module.exports.login = function ( req, res, next ) {
	var user = {};
	var username 	= req.body.username;
	var password 	= req.body.password;
	var pointOfSale = req.body.pointOfSale;
	console.log(req.body)
	if ( !username || !password || !pointOfSale || !req.body.logintoken ) {
		return res.json( 200, {success:false, error: '00003', message: "Missing some info to login"});
	}

	var sql = "select * from credentials.ac_webuser u left join credentials.ac_customer c on ( u.ac_customer_id = c.ac_customer_id ) ";
	sql += " where (c.nickname = $1 or c.customername = $2) and u.userid = $3 ";
	var params = [pointOfSale, pointOfSale, username];
	if( req.body.logintoken ) {
		sql = "select * from credentials.ac_webuser u left join credentials.ac_customer c on ( u.ac_customer_id = c.ac_customer_id ) ";
		sql += " where u.usertoken = $1 ";
		params = [ req.body.logintoken ];
	}
	pg.cnQuery( sql, params , function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			result.success = false;
			result.error = '00003';
			result.message = err;
			return res.json(500, result);
		}

		var rows = queryResult.rows[0];

		if ( !rows ) {
			result.success 	= false;
			result.error 	= '00001';
			result.message 	= "User not found";
			return res.json( 200, result );
		}

		if ( !req.body.logintoken ) {
			if ( rows.pass != crypto.createHash('md5').update(password).digest("hex") ) {
				result.success 	= false;
				result.error 	= '00002';
				result.message 	= "Wrong password";
				return res.json( 200, result );
			}
		}

		var tokenized 	= token.generateToken(rows);
		result.success 	= true;
		result.user 	= rows;
		result.token 	= tokenized;
		return res.json( result );
	});
	
}


// GET 
module.exports.getUserList = function ( req, res, next ) {
	var primary = req.params.primary;
	var sql = "select * from credentials.ac_webuser";
	if ( primary ) {
		sql += " where ac_webuser_id = " + primary
	}
	pg.cnQuery( sql, null, function ( err, queryResult ) {
		if ( err ) {
			console.log(err);
			return res.json(500, []);
		}
		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			console.log( {"error": "something goes wrong", "result": err} );
			return res.json(500, []);
		}
		if ( primary && rows.length == 1 ) {
			rows = rows[0];
		}
		return res.json( 200, rows );
	});
}

module.exports.getGroupList = function ( req, res, next ) {
	var primary = req.params.primary;
	var sql = "select * from credentials.ac_group";
	if ( primary ) {
		sql += " where ac_group_id = " + primary;
	}
	pg.cnQuery( sql, null, function ( err, queryResult ) {
		if ( err ) {
			console.log(err);
			return res.json(500, []);
		}
		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			console.log( { "error": "something goes wrong", "result": err } );
			return res.json(500, []);
		}
		if ( primary && rows.length == 1 ) {
			rows = rows[0];
		}
		return res.json( 200, rows );
	});
}

module.exports.getCustomerList = function ( req, res, next ) {
	var primary = req.params.primary;
	var sql = "select * from credentials.ac_customer";
	if ( primary ) {
		sql += " where ac_customer_id = " + primary;
	}
	pg.cnQuery( sql, null, function ( err, queryResult ) {
		if ( err ) {
			console.log(err);
			return res.json(500, []);
		}
		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			console.log( {"error": "something goes wrong", "result": err} );
			return res.json(500, []);
		}
		if ( primary && rows.length == 1 ) {
			rows = rows[0];
		}
		return res.json( 200, rows );
	});
}

// POST ( update )
module.exports.updateUser = function ( req, res, next ) {

	var primary = req.params.primary;
	var sql 		= "UPDATE credentials.ac_webuser ";
	var set 		= [];
	var queryParams = [];
	set.push( " scope = $1" );
	queryParams.push( params.scope );
	set.push( " relations = $2" );
	queryParams.push( JSON.stringify( params.relations ) );
	set.push( " isonation = $3 " );
	queryParams.push( iso );
	var where = " where ac_abrelations_id = " + primary;

	sql = sql + set.join(",") + where;

	pg.cnQuery( sql, queryParams, function ( err, queryResult ) {
		var result = {};
		if ( err ) {
			return res.json( 500, result );
		}
		return res.json( 200, queryResult );
	});

}

// PUT
module.exports.updateWebUser = function ( req, res, next ) {

	var params = req.body;
	var user = req.params.webuserid;
	var error = {
		code: "0001",
		message: "Missing information"
	}
	if( params.logintoken && user ) {
		var sql = "UPDATE credentials.ac_webuser set usertoken = $1 where ac_webuser_id = $2";
		pg.cnQuery( sql, [params.logintoken, user], function ( err, queryResult ) {
			if ( err ) {
				error.code 		= '0002';
				error.message 	= "Error during update information"
				return res.json( 200, error );
			}
			error.code 		= '0000';
			error.message 	= '';
			return res.json( 200, error );
		})
	} else {
		return res.json( 200, error );
	}
}



