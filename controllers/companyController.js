var pg = require("../libs/query");
var _ = require("underscore");
module.exports.searchCompany = function ( req, res, next ) {
	var term = req.params.term;
	if ( term ) {
		term = term.toUpperCase();
		var sql = "select * from anagrafica.anag_ditta d left join anagrafica.anag_ditta_sedi s on (d.id = s.anadit_id) where";
		var params = [];
		if ( /[a-zA-Z]+/.test( term ) ) { // only code 
			sql += " d.ragione_sociale like $1 ";
			params.push( term + "%" );
		} else {
			sql += " d.ragione_sociale like $1 or d.codice = $2 ";
			params.push( term + "%", term );
		}
	} else {
		return res.json( 200, [] );
	}
	sql += " and d.flg_ann = 'N'";
	pg.query( sql, params, function ( err, queryResult ) {
		if ( err ) {
			console.log( err );
			return res.json( 200, [] );
		}
		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			return res.json( 200, [] );
		}
		var result = [];
		_.each( rows, function ( element, index, list ) {
			result.push({
				"primary": 		element.id,
				"description": 	element.ragione_sociale,
				"code": 		element.codice,
				"fiscalcode": 	element.cod_fiscale,
				"vatid": 		element.partita_iva,
				"giveback": 	element.flg_ass_inde,
				"address":		element.indirizzo,
				"city": 		element.citta,
				"telephone": 	element.telefono,
				"fax": 			element.fax,
				"email": 		element.e_email,
				"labtype": 		null
			});
		})

		return res.json( 200, result );
	})
}
module.exports.getCompanyDetails = function ( req, res, next ) {
	var primary = req.params.primary;
	if ( primary ) {
		var sql = "select * from anagrafica.anag_ditta d left join anagrafica.anag_ditta_sedi s on (d.id = s.anadit_id) where d.id = $1";
		pg.query( sql, [primary], function ( err, queryResult ) {
			if ( err ) {
				console.log( err );
				return res.json( 200, {} );
			}
			var rows = queryResult.rows;
			if ( !_.isArray( rows ) ) {
				return res.json( 200, {} );
			}
			if ( rows.length == 0 ) {
				return res.json( 200, {} );
			}

			var result = {};

			result = {
				"primary": 		rows[0].id,
				"description": 	rows[0].ragione_sociale,
				"code": 		rows[0].codice,
				"fiscalcode": 	rows[0].cod_fiscale,
				"vatid": 		rows[0].partita_iva,
				"giveback": 	rows[0].flg_ass_inde,
				"address":		rows[0].indirizzo,
				"city": 		rows[0].citta,
				"telephone": 	rows[0].telefono,
				"fax": 			rows[0].fax,
				"email": 		rows[0].e_email,
				"labtype": 		null
			};


			return res.json( 200, rows[0] );
		})
	} else {
		return res.json( 200, {} );
	}
}