var pg = require("../libs/query");
var _ = require("underscore");

module.exports.getDeviceList = function ( req, res, next ) {
	var sql = "select * from usersettings.ac_devices";
	pg.cnQuery( sql, null, function ( err, queryResult ) {
		if ( err ) {
			console.log(err);
			return res.json( 200, [] );
		}
		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			return res.json( 200, {} );
		}
		if ( rows.length == 0 ) {
			return res.json( 200, {} );
		}

		_.each( rows ,function ( element, index, list ) {
			rows[index].parameters = JSON.parse( rows[index].parameters );
		})

		return res.json( 200, rows );
	})
}

module.exports.getDevice = function ( req, res, next ) {
	var primary = req.params.primary;
	var sql = "select * from usersettings.ac_devices where id = $1";
	pg.cnQuery( sql, [primary], function ( err, queryResult ) {
		if ( err ) {
			console.log( err );
			return res.json( 200, {} );
		}
		var rows = queryResult.rows;

		if ( !_.isArray( rows ) ) {
			return res.json( 200, {} );
		}
		if ( rows.length == 0 ) {
			return res.json( 200, {} );
		}
		rows[0].parameters = JSON.parse( rows[0].parameters );
		return res.json( 200, rows[0] );
	})
}

module.exports.updateDevice = function ( req, res, next ) {
	var primary = req.params.primary;
}

module.exports.storeDevice = function ( req, res, next ) {

}