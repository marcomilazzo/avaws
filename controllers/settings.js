var pg 		= require("../libs/query");
var _ 		= require("underscore");
var config 	= require("../config");
module.exports.getPreferenceForSync = function ( req, res, next ) {
	
	var sql = "select * from anagrafica.tabella_funzione where flg_tipo = 'O' order by id asc";
	pg.query( sql, null, function ( err, queryResult ) {
		var result = {};
		var rows = queryResult.rows;

		if ( !rows ) {
			result.error 	= '00001';
			result.message 	= "Error getting preference";
			result.data 	= null;
			return res.json( 200, result );
		}

		if ( rows.length == 0 ) {
			result.error 	= '00002';
			result.message 	= "No preference found in table";
			result.data 	= null;
			return res.json( 200, result );
		}

		result.data 	= rows;

		return res.json( result );
	});
}

module.exports.getPanelList = function ( req, res, next ) {
	var sql = "select * from anagrafica.tabella_form where flg_ann <> 'S' and id_padre is not null order by id asc";
	pg.query( sql, null, function ( err, queryResult ) {

		var result 	= {};
		var rows 	= queryResult.rows;
		if ( !_.isArray( rows ) ) {
			result.error 	= '00001';
			result.message 	= "Error getting preference";
			result.data 	= null;
			return res.json( 200, result );
		}

		if ( rows.length == 0 ) {
			result.error 	= '00002';
			result.message 	= "No panel found in table";
			result.data 	= null;
			return res.json( 200, result );
		}

		result.data = rows;

		return res.json( result );

	});
}

module.exports.getGeneralSupplier = function ( req, res, next ) {
	var isonation = req.params.iso
	var sql = "select s.ac_supplier_id as id, s.code as code, s.description as description, tp.description as tp_description";
	sql    += " from usersettings.ac_supplier s, usersettings.ac_transmission_protocol tp ";
	sql    += " where s.isonation = '" + isonation + "'";
	sql    += " and s.ac_transmission_protocol_id = tp.ac_transmission_protocol_id ";
	sql    += " order by s.description asc";

	pg.cnQuery( sql, null, function ( err, queryResult ) {
		var error = {};
		error.code 		= "0001";
		error.message 	= "Error in db query";
		if ( err ) {
			error.result 	= err;
			error.sql 		= sql;
			return res.json( 200, error );
		}

		var rows = queryResult.rows;
		if ( !_.isArray( rows ) ) {
			error.code = '0001';
			error.message = 'Someting goes wrong';
			error.result = [];
			return res.json( 200, error );
		}

		return res.json( 200, rows );

	});
}