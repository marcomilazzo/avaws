alter table farmacia.ftabella_giacenza
add column descrizione character varying( 100 ),
add column codice character varying( 5 ),
add column priorita integer,
add column flg_abilitato integer default 1::integer,
add column flg_principale integer,
drop column tabgia_id,
drop column priorita_acquistato,
drop column priorita_venduto,
drop column flg_giacenza,
drop column flg_inventario,
drop column flg_conf_vendita,
drop column priorita_reso;


alter table vr1881.ftabella_giacenza
add column descrizione character varying( 100 ),
add column codice character varying( 5 ),
add column priorita integer,
add column flg_abilitato integer default 1::integer,
add column flg_principale integer,
drop column tabgia_id,
drop column priorita_acquistato,
drop column priorita_venduto,
drop column flg_giacenza,
drop column flg_inventario,
drop column flg_conf_vendita,
drop column priorita_reso;

alter table spagnatest.ftabella_giacenza
add column descrizione character varying( 100 ),
add column codice character varying( 5 ),
add column priorita integer,
add column flg_abilitato integer default 1::integer,
add column flg_principale integer,
drop column tabgia_id,
drop column priorita_acquistato,
drop column priorita_venduto,
drop column flg_giacenza,
drop column flg_inventario,
drop column flg_conf_vendita,
drop column priorita_reso;

alter table flexifarma.ftabella_giacenza
add column descrizione character varying( 100 ),
add column codice character varying( 5 ),
add column priorita integer,
add column flg_abilitato integer default 1::integer,
add column flg_principale integer,
drop column tabgia_id,
drop column priorita_acquistato,
drop column priorita_venduto,
drop column flg_giacenza,
drop column flg_inventario,
drop column flg_conf_vendita,
drop column priorita_reso;

alter table farmacia.farch_giacenza
add column qta_assinde integer default 0::integer;

alter table spagnatest.farch_giacenza
add column qta_assinde integer default 0::integer;

alter table vr1881.farch_giacenza
add column qta_assinde integer default 0::integer;

alter table flexifarma.farch_giacenza
add column qta_assinde integer default 0::integer;

INSERT INTO anagrafica.tabella_funzione(ID, CODICE, DESCRIZIONE, FLG_TIPO, FLG_AMBITO, VALORE, FLG_ABILITATA, FLG_PASSWORD, COD_GRUPPO, SEQUENZA, FLG_I18N, CHIAVE_I18N, ID_PADRE, FLG_ANN, IMMAGINE, COLORE) VALUES (869, 'F053', 'i18n:ava3.text.magazzini','O','F','stocksSetting','S',null,null,null,null,null,11,'N',null,null);

create or replace function farmacia.checkStocks ( product farmacia.farch_farmacia%rowtype)
returns void as
$BODY$
declare
	stocks cursor from select * from farmacia.ftabella_giacenza where farmacia_id = product.farmacia_id;
	link farmacia.farch_giacenza%rowtype;
begin
	for stock in stocks loop
		select *
		into link
		from farmacia.farch_giacenza where anapro_id_prodotto = product.anapro_id_prodotto and ftagia_id = stock.id and farmacia_id = product.farmacia_id;
		IF NOT FOUND THEN
			insert into farmacia.farch_giacenza(id, anapro_id_prodotto, ftagia_id, flg_ann, farmacia_id )
			values ( (SELECT coalesce(max(id),0)+1 FROM farmacia.farch_giacenza), product.anapro_id_prodotto, stock.id, 'N', product.farmacia_id )
		END IF; 
	end loop;
end;
$BODY$ language plpgsql;

create or replace function farmacia.syncStock()
returns void as
$BODY$
declare
	products cursor for select * from farmacia.farch_farmacia;
begin
	for product in products loop
		checkStocks( product );
	end loop;
end;
$BODY$ language plpgsql;



create type anagrafica.productcounters as (
	companyid integer,
	company integer,
	compositionid integer,
	composition integer
);

create or replace function anagrafica.productSidebar(primarykey integer)
returns anagrafica.productcounters
as
$$
declare
	productcounters anagrafica.productcounters%rowtype;
	productinfo = anagrafica.productinfo$rowtype;
begin
	
	productinfo = getproduct( primarykey );
	productcounters.companyid = productinfo.companyid;
	productcounters.compositionid = productinfo.compositionid;
	select count(id_prodotto) 
	into productcounters.company
	from anagrafica.anag_prodotti where anadit_id =  productinfo.companyid;

	select count( id_prodotto )
	into productcounters.composition
	from anagrafica.anag_prodotti where anapri_id = productinfo.compositionid;

	return productcounters;
end
$$ language plpgsql;