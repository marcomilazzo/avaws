module.exports = {
	'secret': "apikeysecretverifystring",
	'postgres': {
		'user': 'postgres', //env var: PGUSER
		'database': 'AVA3', //env var: PGDATABASE
		'password': 'Oracle000', //env var: PGPASSWORD
		'port': 5433, //env var: PGPORT
		'host': "192.168.254.85",
		'max': 100, // max number of clients in the pool
		'idleTimeoutMillis': 20000, // how long a client is allowed to remain idle before being closed
	},
	'postgrescentralcn': {
		'user': 'postgres', //env var: PGUSER
		'database': 'ava3central', //env var: PGDATABASE
		'password': 'Oracle000', //env var: PGPASSWORD
		'port': 5433, //env var: PGPORT
		'host': "192.168.254.85",
		'max': 100, // max number of clients in the pool
		'idleTimeoutMillis': 20000, // how long a client is allowed to remain idle before being closed
	},
	'postgrescentralcncs': "pg://postgres:Oracle000@localhost:5433/ava3central"
}
