var restify = require("restify");
var routes 	= require("./routes");
var auth 	= require("./libs/auth");
var config 	= require("./config");

var server = restify.createServer({
  name: 'avaws',
  version: '0.9.1'
});

server.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization, x-access-token');
	res.setHeader('Access-Control-Allow-Methods', '*');
	res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
	res.setHeader('Access-Control-Max-Age', '1000');
	next();
});


server.use( restify.acceptParser( server.acceptable ) );
server.use( restify.queryParser() );
server.use( restify.bodyParser({ mapParams: false }) );

routes.set( server );

var listenHandler = function ( ) {
	console.log( "restify %s listen on port %s", server.name, server.url );
}

server.listen( 3650, listenHandler );